#!/usr/bin/env python3

import shutil, sys, time
import re, os.path
import subprocess


## == LOOP == ##
class TimerBar:
    # COLOR THEME
    LC,RC = '\x1b[1;31m▕\x1b[0m', '\x1b[1;31m▏\x1b[0m'      # red/white
    #LC,RC = '\x1b[1;34m▕\x1b[1;30m', '\x1b[1;34m▏\x1b[0m'  # blue/gray   
    BC = '█▉▊▋▌▍▎▏'
    HAND = '|/-\\'

    def __init__(self):
        pass

    def progress_bar(self, t: float, tot: int, bw=None) -> str:
        if bw is None:
            bw = shutil.get_terminal_size()[0] - 21
        m,s = divmod(int(t), 60)
        M,S = divmod(tot, 60)
        blf = bw*t/tot
        bli = int(blf)
        blr = int(8*(blf-bli))
        ec = ''
        if blr > 0:
            ec = self.BC[8-blr]
        bar = (self.BC[0]*bli + ec).ljust(bw)
        pc = int(100.*t/tot)
        hand = self.HAND[s % len(self.HAND)]
        line = f'{hand}{self.LC}{bar}{self.RC}{pc:3d}%  {m:02d}:{s:02d}/{M:02d}:{S:02d}'
        # alternative format:
        #line = f'{hand} {m:02d}:{s:02d}{self.LC}{bar}{self.RC}{M:02d}:{S:02d} [{pc:3d}%]'
        return line

    def timer_loop(self, dt):
        t0 = t1 = time.time()
        prev_bar = None
        while t1-t0 < dt:
            bar = self.progress_bar(t1-t0, dt)
            if bar != prev_bar:
                print(bar, end='\r', flush=True)
                prev_bar = bar
            time.sleep(1/8)
            t1 = time.time()
        print(self.progress_bar(dt, dt))


## == BEEP == ##
BEEP_FILE = os.path.join(os.path.dirname(__file__), 'rings', 'beep.ogg')

def print_beep(t):
    subprocess.run(('figlet', '-c', '< < Driiing > >'))
    subprocess.run(('figlet', '-f', 'small', '-c', f'+ {round(t)} s'))

def play_beep():
    cmd = ('ffplay', '-nodisp', '-autoexit', '-hide_banner', BEEP_FILE)
    # alternative without ffmpeg (with alsa & wav format):
    #cmd = ('aplay', '-q', BEEP_FILE)
    subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

def beep_loop():
    t0 = time.time()
    while True:
        print_beep(time.time()-t0)
        play_beep()
        time.sleep(1)


## == ARGS == ##
def arg_error():
    print(f'usage: {sys.argv[0]} mm:ss', file=sys.stderr)
    sys.exit(1)

def arg_parse():
    arg = ' '.join(sys.argv[1:])
    TO = re.fullmatch(r'(?P<min>\d+)(?:[m: ](?P<sec>\d+)s?)?', arg)
    if TO:
        m = int(TO.group('min'))
        s = 0
        if TO.group('sec') is not None:
            s = int(TO.group('sec'))
    else:
        arg_error()
    t = 60*m + s
    return t


## == MAIN == ##
def main():
    t = arg_parse()
    bar = TimerBar()
    try:
        bar.timer_loop(t)
        print()
        beep_loop()
    except KeyboardInterrupt:
        print()
        sys.exit()

if __name__=='__main__':
    main()
